<?php
defined( 'PLUGIN_ACCESS_FLAG' ) or die( 'Go away!' );
if ( ! class_exists( 'SiteDuplicatorLibDb' ) ) {
    class SiteDuplicatorLibDb
    {

        public static function getStats(){
            global $wpdb;

            $data = array(
                'tables'        => array(),
                'count_tables'  => 0,
                'size_tables'   => 0,
            );
            $data['tables']         = self::getTables();
            $data['count_tables']   = @(int)count($data['tables']);
            if( !empty($data['tables']) ){
                foreach($data['tables'] as $k_item => $v_item){
                    $data['size_tables'] += $v_item['size'];
                }
            }

            return $data;
        }

        public static function getTables(){
            global $wpdb;

            $list = array();

            $tables	 = $wpdb->get_results("SHOW TABLE STATUS", ARRAY_A);
            if( !empty($tables) ){
                foreach($tables as $k_item => $v_item){
                    $list[] = array(
                        'name'      => $v_item['Name'],
                        'size'      => $v_item['Data_length'],
                        'rows'      => $v_item['Rows'],
                        'collation' => $v_item['Collation'],
                    );
                }
            }

            return $list;
        }

        public static function getTableStructure($table){
            global $wpdb;

            $data = $wpdb->get_row("SHOW CREATE TABLE `{$table}`", ARRAY_N);
            $data = @$data[1];

            return $data;
        }

        public static function getTableInsert($table){
            global $wpdb;

            $data   = '';

            $count = $wpdb->get_var('SELECT count(*) FROM `'.$table.'`');
            if ($count > 100) {
                $count = ceil($count / 100);
            } else {
                $count = ($count > 0 ? 1 : $count);
            }

            for ($i = 0; $i < $count; $i++) {
                $low_limit = $i * 100;
                $sqlQuery  = 'SELECT * FROM `'.$table.'` LIMIT '.(int)$low_limit.', 100';
                $rows      = $wpdb->get_results($sqlQuery, ARRAY_A);
                if (is_array($rows)) {
                    foreach ($rows as $row) {
                        //insert single row
                        $data  .= 'INSERT INTO `'.$table.'` VALUES(';
                        $num_values = count($row);
                        $j          = 1;
                        foreach ($row as $value) {
                            $value = addslashes($value);
                            $value = preg_replace("/\n/Ui", "\\n", $value);
                            $num_values == $j ? $data .= "'".$value."'" : $data .= "'".$value."', ";
                            $j++;
                            unset($value);
                        }
                        $data .= ");\n";
                    }
                }
            }

            return $data;
        }

        public static function dumpTable($filePath, $table){

            $structure  = self::getTableStructure($table);
            $insert     = self::getTableInsert($table);

            $data = '';
            $data .= 'SET FOREIGN_KEY_CHECKS = 0;'."\n\n";
            $data .= 'DROP TABLE IF EXISTS `'.$table.'`;'."\n";
            $data .= rtrim($structure,';').';'."\n\n";
            $data .= $insert."\n\n";
            $data .= 'SET FOREIGN_KEY_CHECKS = 1;'."\n";

            $dumpRes = SiteDuplicatorLibFilesystem::writeContentFile($filePath,$data);
            if( @(int)$dumpRes['status'] == 1){
                $dumpRes = SiteDuplicatorLibUtility::logEvent(22, $table);
            }else{
                $dumpRes = SiteDuplicatorLibUtility::logEvent(23, $table);
            }

            return $dumpRes;
        }
    }
}