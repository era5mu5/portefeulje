<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content div and all content after
 */

?>

</div><!-- .site-content -->

<footer id="footer" role="contentinfo">

<div class="container">

	<?php
		$footer_sections = 0;
		$tania_address = get_theme_mod('tania_address',__('Taft Street, Brooklyn, Wellington','tania-lite'));
		$tania_address_icon = get_theme_mod('tania_address_icon',get_template_directory_uri().'/images/map25-redish.png');
		
		$tania_email = get_theme_mod('tania_email','<a href="mailto:taniaerasmus001@gmail.com">taniaerasmus001@gmail.com</a>');
		$tania_email_icon = get_theme_mod('tania_email_icon',get_template_directory_uri().'/images/envelope4-green.png');
		
		$tania_phone = get_theme_mod('tania_phone','<a href="tel:027 302 3515">027 302 3515</a>');
		$tania_phone_icon = get_theme_mod('tania_phone_icon',get_template_directory_uri().'/images/telephone65-blue.png');

		$tania_socials_facebook = get_theme_mod('tania_socials_facebook','#');
		$tania_socials_twitter = get_theme_mod('tania_socials_twitter','#');
		$tania_socials_linkedin = get_theme_mod('tania_socials_linkedin','#');
		$tania_socials_behance = get_theme_mod('tania_socials_behance','#');
		$tania_socials_dribbble = get_theme_mod('tania_socials_dribbble','#');
		
		$tania_accessibility = get_theme_mod('tania_accessibility');
		$tania_copyright = get_theme_mod('tania_copyright');

		if(!empty($tania_address) || !empty($tania_address_icon)):
			$footer_sections++;
		endif;
		
		if(!empty($tania_email) || !empty($tania_email_icon)):
			$footer_sections++;
		endif;
		
		if(!empty($tania_phone) || !empty($tania_phone_icon)):
			$footer_sections++;
		endif;
		if(!empty($tania_socials_facebook) || !empty($tania_socials_twitter) || !empty($tania_socials_linkedin) || !empty($tania_socials_behance) || !empty($tania_socials_dribbble) || 
		!empty($tania_copyright)):
			$footer_sections++;
		endif;
		
		if( $footer_sections == 1 ):
			$footer_class = 'col-md-12';
		elseif( $footer_sections == 2 ):
			$footer_class = 'col-md-6';
		elseif( $footer_sections == 3 ):
			$footer_class = 'col-md-4';
		elseif( $footer_sections == 4 ):
			$footer_class = 'col-md-3';
		else:
			$footer_class = 'col-md-3';
		endif;
		
		/* COMPANY ADDRESS */
		if( !empty($tania_address) ):
			echo '<div class="'.$footer_class.' company-details">';
				echo '<div class="icon-top red-text">';
					if( !empty($tania_address_icon) ) echo '<img src="'.esc_url($tania_address_icon).'" alt="" />';
				echo '</div>';
				echo $tania_address;
			echo '</div>';
		endif;
		
		/* COMPANY EMAIL */
		
		
		if( !empty($tania_email) ):
			echo '<div class="'.$footer_class.' company-details">';
				echo '<div class="icon-top green-text">';
					
					if( !empty($tania_email_icon) ) echo '<img src="'.esc_url($tania_email_icon).'" alt="" />';
				echo '</div>';
				echo $tania_email;
			echo '</div>';
		endif;
		
		/* COMPANY PHONE NUMBER */
		
		
		if( !empty($tania_phone) ):
			echo '<div class="'.$footer_class.' company-details">';
				echo '<div class="icon-top blue-text">';
					if( !empty($tania_phone_icon) ) echo '<img src="'.esc_url($tania_phone_icon).'" alt="" />';
				echo '</div>';
				echo $tania_phone;
			echo '</div>';
		endif;
		
		// open link in a new tab when checkbox "accessibility" is not ticked
		$attribut_new_tab = (isset($tania_accessibility) && ($tania_accessibility != 1) ? ' target="_blank"' : '' );
		
		if( !empty($tania_socials_facebook) || !empty($tania_socials_twitter) || !empty($tania_socials_linkedin) || !empty($tania_socials_behance) || !empty($tania_socials_dribbble) || 
		!empty($tania_copyright)):
		
					echo '<div class="'.$footer_class.' copyright">';
					if(!empty($tania_socials_facebook) || !empty($tania_socials_twitter) || !empty($tania_socials_linkedin) || !empty($tania_socials_behance) || !empty($tania_socials_dribbble)):
						echo '<ul class="social">';
						
						/* facebook */
						if( !empty($tania_socials_facebook) ):
							echo '<li><a'.$attribut_new_tab.' href="'.esc_url($tania_socials_facebook).'"><i class="fa fa-facebook"></i></a></li>';
						endif;
						/* twitter */
						if( !empty($tania_socials_twitter) ):
							echo '<li><a'.$attribut_new_tab.' href="'.esc_url($tania_socials_twitter).'"><i class="fa fa-twitter"></i></a></li>';
						endif;
						/* linkedin */
						if( !empty($tania_socials_linkedin) ):
							echo '<li><a'.$attribut_new_tab.' href="'.esc_url($tania_socials_linkedin).'"><i class="fa fa-linkedin"></i></a></li>';
						endif;
						echo '</ul>';
					endif;	
			
			
					if( !empty($tania_copyright) ):
						echo esc_attr($tania_copyright);
					endif;
					
					echo '<div class="tania-copyright-box">Copyright © 2016 - Tania Erasmus </a>'.__('powered by','tania-lite').'<a class="tania-copyright" href="http://wordpress.org/"'.$attribut_new_tab.' rel="nofollow"> WordPress</a></div>';
					
					echo '</div>';
			
		endif;
	?>

</div> <!-- / END CONTAINER -->

</footer> <!-- / END FOOOTER  -->


	</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


<?php wp_footer(); ?>

</body>

</html>